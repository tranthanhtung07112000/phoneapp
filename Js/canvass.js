var mycanvas = document.getElementById("myCanvas2");
var context = mycanvas.getContext("2d");
var value = 30;
var value1 = "C";
var value2 = "SET TEMPERATURE";
var x = mycanvas.width / 2;
var y = mycanvas.height / 2;
var xm = x - 200;
var ym = y;
var xp = x + 200;
var yp = y;
var r = 100;
var startangle = (-9 * Math.PI) / 8;
var endangle = (1 * Math.PI) / 8;
var clockwise = false;
//mau gradient
var grd = context.createLinearGradient(288, 230, 350, 230); //gradient xanh trang (truyền vào tọa độ điểm bắt đầu và tọa độ điểm kết thúc)
grd.addColorStop(0, "#1ec1a4"); //( vị trí hiển thị màu tương ứng , màu tương ứng)
grd.addColorStop(0.15, "#1ec1a4");
grd.addColorStop(0.95, "#1ec1a4");
grd.addColorStop(1, "#1ec1a4");
var grd2 = context.createLinearGradient(210, 123, 320, 200); //gradient xanh trang
grd2.addColorStop(0, "#1ec1a4");
grd2.addColorStop(0.8, "#1ec1a4");
grd2.addColorStop(0.9, "#1ec1a4");
//khoi tao
//ve cac nut +/- va nen cua round
context.beginPath(); //ve nut -
context.arc(xm + 100, ym + 90, 20, 0, 2 * Math.PI, clockwise); // xm,ym: tọa độ tâm của đường trong ; 20 : bán kính của đường tròn; 0: góc bắt đầu;2 *math.PI: góc kết thúc; clockwise: chọn chiều
context.lineWidth = 10; // thiết lập độ rộng của đường kẻ hiện tại
context.strokeStyle = "#e9eaf3"; //xét màu cho viền , có thể chọn màu gardient
context.fillStyle = "#e9eaf3"; // thiết lập màu đổ lên hình
context.fill(); //đổ màu lên hình hiện tại ,chú ý là phải đóng clsepath()
context.stroke(); // hiển thị nét vẽ
context.closePath(); // dùng để tạo nét vẽ từ vị trí hiện tại về điểm xuất phát nét vẽ
drawLine(xm + 92, ym + 90, xm + 108, ym + 90, "#000", 1); // vẽ đường thẳng với màu
context.beginPath(); //ve nut +
context.arc(xp - 100, yp + 90, 20, 0, 2 * Math.PI, clockwise);
context.lineWidth = 10;
context.strokeStyle = "#e9eaf3";
context.fillStyle = "#e9eaf3";
context.fill();
context.stroke();
context.closePath();

drawLine(xp - 108, yp + 90, xp - 92, yp + 90, "#000", 1);
drawLine(xp - 100, yp + 82, xp - 100, yp + 98, "#000", 1);
value_draw(value); //ve round , vẽ số 64
value_writeee(value1);
value_writee(value2);
draw_background_value();

mycanvas.addEventListener("mousedown", (e) => {
  px = e.offsetX;
  py = e.offsetY;
  console.log("x position:%d y position: %d", px, py);
  if (e.offsetY >= y + 65 && e.offsetY <= y + 115)
    if (e.offsetX >= xm + 75 && e.offsetX <= xm + 125) {
      value = value - 1;
      console.log("tru");
    } else if (e.offsetX >= xp - 125 && e.offsetX <= xp + 75) {
      value = value + 1;
      console.log("cong");
    }
  if (value > 100) {
    value = 100;
  } else if (value < 0) {
    value = 0;
  }
  value_draw(value);
  value_writeee(value1);
});

function draw_background() {
  context.beginPath();
  context.fillStyle = "#ffffff";
  context.fillRect(0, 0, 480, 230); // vẽ 1 hình chữ nhật đã được tô màu ; phải có fillstyle trước
  context.stroke();
  context.arc(x, y - 60, r, startangle, endangle, clockwise); // vẽ 1 hình chữ nhật từ tâm
  context.lineWidth = 10; // độ rộng của đường vẽ là 10
  context.strokeStyle = "#e9eaf3"; // set màu cho đường vẽ
  context.stroke();
  context.closePath();
}

function value_write(val) {
  context.beginPath();
  context.font = "25px MS UI Gothic"; // set font chữ cho số giá trị hiển thị
  context.fillStyle = "#1ec1a4"; // màu
  context.textAlign = "center"; // căn lề
  context.fillText(val, x - 5, y - 50); // vẽ chữ đã được đổ màu trong canvas (val: nội dung hiển thị,x: tọa độ điểm x để vẽ, y+30 tọa độ điểm y, có thể thêm độ dài lớn nhất cảu chữ)
  context.closePath();
}

function value_writee(val) {
  context.beginPath();
  context.font = "15px MS UI Gothic"; // set font chữ cho số giá trị hiển thị
  context.fillStyle = "#1ec1a4"; // màu
  context.textAlign = "center"; // căn lề
  context.fillText(val, x, y + 100); // vẽ chữ đã được đổ màu trong canvas (val: nội dung hiển thị,x: tọa độ điểm x để vẽ, y+30 tọa độ điểm y, có thể thêm độ dài lớn nhất cảu chữ)
  context.closePath();
}

//vẽ thêm chữ
function value_writeee(val) {
  context.beginPath();
  context.font = "15px MS UI Gothic"; // set font chữ cho số giá trị hiển thị
  context.fillStyle = "1ec1a4"; // màu
  context.textAlign = "center"; // căn lề
  context.fillText(val, x + 15, y - 50); // vẽ chữ đã được đổ màu trong canvas (val: nội dung hiển thị,x: tọa độ điểm x để vẽ, y+30 tọa độ điểm y, có thể thêm độ dài lớn nhất cảu chữ)
  context.closePath();
}

function draw_point(val_ang, R) {
  var dx = Math.cos(val_ang) * R;
  var dy = Math.sin(val_ang) * R;
  context.beginPath(); //vong trang
  context.arc(x + dx, y + dy - 60, 14, 0, 2 * Math.PI, clockwise);
  context.lineWidth = 1;
  context.fillStyle = "#ffffff";
  context.shadowColor = "#d2d4d6";
  context.shadowBlur = 4;
  context.shadowOffsetX = 0;
  context.shadowOffsetY = 3;
  context.fill();
  context.strokeStyle = "#ffffff";
  context.stroke();
  context.closePath();
  context.shadowColor = "#ffffff";
  context.shadowBlur = 0;
  context.shadowOffsetX = 0;
  context.shadowOffsetY = 0;
  context.beginPath(); //vong xanh duong
  context.arc(x + dx, y + dy - 60, 7, 0, 2 * Math.PI, clockwise);
  context.lineWidth = 1;
  context.fillStyle = "#3e78e0";
  context.fill();
  context.strokeStyle = "#3e78e0";
  context.stroke();
  context.closePath();
}

function value_draw(dvalue) {
  draw_background();
  draw_dotted_round(dvalue);
  var valueangle = ((dvalue - 90) * Math.PI) / 80;
  context.beginPath();
  context.arc(x, y - 60, r, startangle, valueangle, clockwise);
  context.lineWidth = 15;
  context.strokeStyle = grd;
  context.stroke();
  context.closePath();
  value_write(dvalue);
  draw_point(valueangle, r);
}

function drawLine(dlx1, dly1, dlx2, dly2, draw_color, line_width) {
  context.beginPath();
  context.strokeStyle = draw_color;
  context.lineWidth = line_width;
  context.moveTo(dlx1, dly1);
  context.lineTo(dlx2, dly2);
  context.stroke();
  context.closePath();
}

function draw_dotted_round(value) {
  const r1 = 120;
  const r2 = 130;
  var temp = Math.PI / 80;
  for (var i = -90; i <= 10; i++) {
    var px1 = r1 * Math.cos(i * temp);
    var py1 = r1 * Math.sin(i * temp);
    var px2 = r2 * Math.cos(i * temp);
    var py2 = r2 * Math.sin(i * temp);
    drawLine(x + px1, y + py1 - 60, x + px2, y + py2 - 60, "#e9eaf3", 2);
  }
  for (var i = -90; i <= value - 90; i++) {
    var vx1 = (r1 - 1) * Math.cos(i * temp);
    var vy1 = (r1 - 1) * Math.sin(i * temp);
    var vx2 = (r2 + 1) * Math.cos(i * temp);
    var vy2 = (r2 + 1) * Math.sin(i * temp);
    drawLine(x + vx1, y + vy1 - 60, x + vx2, y + vy2 - 60, grd2, 2);
  }
}
//vẽ khung cho giá trị
function draw_background_value() {
  context.beginPath();
  context.strokeStyle = "#1ec1a4";
  context.arc(140, 305, 50, 0.5 * Math.PI, 1.5 * Math.PI, clockwise);
  context.moveTo(140, 355);
  context.lineTo(335, 355);
  context.arc(335, 305, 50, 0.5 * Math.PI, 1.5 * Math.PI, true);
  context.moveTo(335, 255);
  context.lineTo(140, 255);
  context.lineWidth = 2; // độ rộng của đường vẽ là 10
  context.stroke();
  context.closePath();
}
